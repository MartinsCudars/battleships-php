<?php 
    require 'lib/config.php';
    require 'lib/game.php';
    require 'lib/gamestate.php';
    require 'lib/grid.php';
    require 'lib/ships.php';

    $game = new game();
    $gameState = new gamestate();
    $game -> gameTurn();
?>

<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="manifest" href="site.webmanifest">
        <link rel="apple-touch-icon" href="icon.png">
        <!-- Place favicon.ico in the root directory -->
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/battleships.css">
    </head>
    <body>
        <main class="main">
            <h1 class="main__title">Battleships</h1>
            <div class="main__content">
                <div class="battle-grid">
                    <?php $game->renderGame(); ?>
                </div>
                <div class="sidebar">
                    <h2>Legend</h2>
                    <ul class="legend">
                        <li><span class="legend-label">Empty:</span><span class="legend-item legend-item--empty"></span></li>
                        <li><span class="legend-label">Ship:</span><span class="legend-item legend-item--ship"></span></li>
                        <li><span class="legend-label">Miss:</span><span class="legend-item legend-item--miss"></span></li>
                        <li><span class="legend-label">Hit: </span><span class="legend-item legend-item--hit"></span></li>
                    </ul>
                    <a href="newgame.php" class="btn btn--blue">Restart Game</a>
                </div>
            </div>
        </main>


        <?php if($gameState->getGameLives() === 0): ?>
            <div class="modal">
                <h2>Congratulations, you did it!</h2>
                <p>You shot <?php echo $gameState->getShotsFired(); ?> times and managed to get them all.</p>
                <a href="newgame.php" class="btn btn--blue">Start new Game</a>
            </div>
        <?php endif; ?>


        <section class="battle-controls">
            <div class="container">
                <div class="item">
                    LIVES: <?php echo $gameState->getGameLives(); ?>
                </div>
                <div class="item">
                    SHOTS FIRED: <?php echo $gameState->getShotsFired(); ?>
                </div>

                <?php if($gameState->getGameLives() > 0): ?>
                    <a href="index.php" class="btn btn--red last-item">Shoot</a>
                <?php endif; ?>
            </div>
        </section>

    </body>
</html>