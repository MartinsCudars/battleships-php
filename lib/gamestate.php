<?php 

/**
 * Utilities For saving / loading data from session
 */
class gameState {

	public function initGameState() {
		$_SESSION['gameData'] = [];
		$_SESSION['gameData']['gameLives'] = 0;
 		$_SESSION['gameData']['shotCount'] = 0;
	}

		// if game exists, return true
	public function gameStatus() {
		if( isset($_SESSION['gameData'])) {
			return true;
		} else {
			return false;
		}
	}

	public function endGame() {
		session_start();
		session_destroy(); 
	}

	public function saveGrid($gridData) {
		$_SESSION['gameGrid'] = $gridData;
	}

	public function getGrid() {
		return $_SESSION['gameGrid'];
	}

	public function increaseShotsFired() {
		$_SESSION['gameData']['shotCount']++;
	}
	public function getShotsFired() {
		return $_SESSION['gameData']['shotCount'];
	}

	public function getGameLives() {
		return $_SESSION['gameData']['gameLives'];
	}
	public function addGameLives($amount) {
		$_SESSION['gameData']['gameLives'] = $_SESSION['gameData']['gameLives'] + $amount;
	}

	public function removeGameLives($amount) {
		$_SESSION['gameData']['gameLives'] = $_SESSION['gameData']['gameLives'] - $amount;
	}

	public function createTargetMap() {
		$gameGrid = new grid();
		$gameGrid->setGridData($_SESSION['gameGrid']);
		$gameGridData = $gameGrid->getGridData();

		foreach ($gameGridData as $rowKey => $rowValue) {
			foreach($rowValue as $colKey => $colValue) {
				$targetMap[] = [$rowKey, $colKey];
			}
		}

		self::setTargetMap($targetMap);

	}

	// Save and load target map, needed for tracking which tiles user has already hit

	public function getTargetMap() {
		return $_SESSION['gameData']['targetMap'];
	}

	public function setTargetMap($newTargetMap) {
		$_SESSION['gameData']['targetMap'] = $newTargetMap;
	}

	public function removeFromTargetMap($target) {
		$targetMap = self::getTargetMap();
		unset($targetMap[$target]);
		$reindexTargetMap = array_values($targetMap);
		self::setTargetMap($reindexTargetMap);
	}
}