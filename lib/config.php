<?php

class config {
	// grid size - in theory we can adjust this, we only need to update css grid to have more rows/cols
	const GRID_SIZE = 10;

	// grid cell status
	const GRID_EMPTY = 0;
	const GRID_SHIP = 1;
	const GRID_RESERVED = 2;
	const GRID_MISS = 3;
	const GRID_HIT = 4;


}