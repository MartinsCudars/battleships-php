<?php
	class grid {
		var $rows = config::GRID_SIZE;
		var $cols = config::GRID_SIZE;
		var $grid = [];

		function generateGrid() {
			for ($row = 0; $row < $this->rows; $row++) {
				$gridColData = [];
				for($col = 0; $col < $this->cols; $col++) {
					$gridColData[] = 0;
				}
				$gridData[] = $gridColData;
			}
			$this->grid = $gridData;	
		}

		function getGridData() {
			return $this->grid;
		}

		// load grid data
		function setGridData($gridData) {
			$this->grid = $gridData; 
		}

		function saveGridData() {
			gamestate::saveGrid($this->getGridData());
		}

		function renderGrid() {
			$rowIndex = 0;
			foreach ($this->grid as $gridRow) {
				$colIndex = 0;
				foreach ($gridRow as $gridCell) {
					// echo '<div class="grid-cell grid-cell-'. $gridCell.'" data-row="'. $rowIndex . '" data-col="' . $colIndex . '"><span class="grid-cell-id">' . $rowIndex . '/' . $colIndex . '</span>' . $gridCell .'</div>';
					echo '<div class="grid-cell grid-cell-'. $gridCell.'" data-row="'. $rowIndex . '" data-col="' . $colIndex . '"></div>';
					$colIndex++;
				}
				$rowIndex++;	
			}	
		}

		function updateGridCell($targetRow, $targetCol, $newStatus) {
			$this->grid[$targetRow][$targetCol] = $newStatus;
		}
	}

?>