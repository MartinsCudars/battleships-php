<?php

	class ship {
		public $shipLives;
		public $shipShape;

		public function getShipShape() {
			return $this->shipShape;
		}

		public function setShipShape($shape) {
			$this->shipShape = $shape;
		}

		public function getShipLives() {
			return $this->shipLives;
		}

		public function damageShip() {
			if( $shipLives > 0) {
				$shipLives = $shipLives - 1;
			}
		}


		public function placeShip($gridData, $posY, $posX) {
			$this->randomRotation();
			$shape = $this->getShipShape();
			$cellsCount = $this->countShipCells();
			$height = $cellsCount[0];
		    $width = $cellsCount[1];
		    
		    $currentY = $posY;
			
		    for ($col = 0; $col < $height; $col++) {
					
				$currentX = $posX;
		    	for ($row = 0; $row < $width; $row++) {
    				$gridData[$currentY][$currentX] = $shape[$col][$row];
		    		$currentX++;
		    	}
				$currentY++;	    	 
		    }

			return $gridData;
		}

		public function countShipCells() {
			$shape = $this->getShipShape();
			$rows = count($shape);
			$cols = count($shape[0]);	
			return [$rows, $cols];
		}

		public function rotateShip() {

			$originalShape = $this->getShipShape();
			$cellsCount = $this->countShipCells();
			$height = $cellsCount[0];
		    $width = $cellsCount[1];

		    $rotatedShape = [];

			$rotatedShape = call_user_func_array(
			    'array_map',
			    array(-1 => null) + array_map('array_reverse', $originalShape)
			);

		    $this->setShipShape($rotatedShape);
		}

		public function randomRotation() {
			$rotationCount = rand(0, 3);
			// $rotationCount = 0;
			for ($i=0; $i < $rotationCount; $i++) { 
				$this->rotateShip();
			}
		}
	}

	// L shape ship, 
	class carrier extends ship {
		public $shipLives = 4;
		
		public $shipShape = 
		[
			[2, 2, 2, 0],
			[2, 1, 2, 0],
			[2, 1, 2, 2],
			[2, 1, 1, 2],
			[2, 2, 2, 2]
		];

	}
	// I shape ship
	class destroyer extends ship {
		public $shipLives = 4;

		public $shipShape =
		[
			[2, 2, 2],
			[2, 1, 2],
			[2, 1, 2],
			[2, 1, 2],
			[2, 1, 2],
			[2, 2, 2]
		];

	}

	// Dot shape ship
	class submarine extends ship {
		public $shipLives = 1;

		public $shipShape = 
		[
			[2, 2, 2],
			[2, 1, 2],
			[2, 2, 2]
		];

	}


?>