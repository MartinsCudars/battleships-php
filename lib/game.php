<?php

class game {

	private $gameData;
	private $gameGrid;

	// Init game, setup session, grid, targetmap
	function newGame() {
		gamestate::initGameState();
		$gameGrid = new grid();
		$gameGrid->generateGrid();
        $this->setupShips($gameGrid);
		gamestate::saveGrid($gameGrid->getGridData());
		gamestate::createTargetMap();
	}


	// show grid in frontend
	function renderGame() {
		if( gamestate::getGameLives() !== 0) {
			$gameGrid = new grid();
        	$gameGrid->setGridData( gamestate::getGrid() );
        	$gameGrid->renderGrid();
	    }
	}

	// just for debuging
	function displayGameData() {
		print_r($gameGrid->getGridData());
	}

	// start session each turn, check if game is still going
	function gameTurn() {
		session_start();

		if(gamestate::gameStatus()) {
			$this->gameAction();
		} else {
			$this->newGame();
		}
	
	}

	// setup all ships on the grid

	function setupShips($gameGrid) {

		$carrier = new carrier();
		$gridData = $carrier->placeShip($gameGrid->getGridData(), 0, 1);
		$gameGrid->setGridData($gridData);
		gamestate::addGameLives($carrier->getShipLives());

		$destroyer = new destroyer();
		$gridData = $destroyer->placeShip($gameGrid->getGridData(), 4, 1);
		$gameGrid->setGridData($gridData);
		gamestate::addGameLives($destroyer->getShipLives());

		$sub1 = new submarine();
		$gridData = $sub1->placeShip($gameGrid->getGridData(), 5, 6);
		$gameGrid->setGridData($gridData);
		gamestate::addGameLives($sub1->getShipLives());

		$sub2 = new submarine();
		$gridData = $sub2->placeShip($gameGrid->getGridData(), 7, 7);
		$gameGrid->setGridData($gridData);
		gamestate::addGameLives($sub2->getShipLives());

	}

	function gameShoot($targetY, $targetX) {
		$min = 0;
		$max = config::GRID_SIZE;

		if( (($min <= $targetY) && ($targetY <= $max)) && (($min <= $targetX) && ($targetX <= $max))  ) {
			$gameGrid = new grid();
			$gameGrid->setGridData(gamestate::getGrid());
			$gameGridData = $gameGrid->getGridData();

			switch($gameGridData[$targetY][$targetX]) {
				case 1:
					$gameGrid->updateGridCell($targetY, $targetX, 4);
					gamestate::removeGameLives(1);
					break;
				case 4:
					return false;
					break;
				default: 
					$gameGrid->updateGridCell($targetY, $targetX, 3);
					break;

			}

			$gameGrid->saveGridData();
			gamestate::increaseShotsFired();
		}
	}

	function gameAction() {
		if(gamestate::getGameLives() > 0) {
			$targetMap = gamestate::getTargetMap();
			$randomTargetIndex = rand(0, (count($targetMap) - 1));
			$randomTarget = $targetMap[$randomTargetIndex];
			$this->gameShoot($randomTarget[0], $randomTarget[1]);
			gamestate::removeFromTargetMap($randomTargetIndex);	
		}

	}
}

?>