<?php

	require 'lib/game.php';
	require 'lib/gamestate.php';

	$gameState = new gameState();

	$gameState->endGame();

	header('Location: index.php');

?>